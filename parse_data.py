from pathlib import Path
from dataclasses import dataclass
from enum import Enum

from pydantic import BaseModel


class Tier(Enum):
    T0 = "T0"
    T1 = "T1"
    T2 = "T2"
    T3 = "T3"

class Aa_gun(BaseModel):
    name : str
    tier: Tier
    anti_air : int
    dmg: int
    rld : float
    rng : int



def parse_equipment(file: Path):
    with open(file, mode='r') as _data:
        data_lines = _data.readlines()

    (l.split() for l in data_lines)

    i = 0

    while len(data_lines) > 0 or i < 4:
        i = i + 1

        chunk = data_lines[0:4]

        split_lines = [l.replace('\n', '').split('\t') for l in chunk]

        if len(split_lines[3]) == 8:
            split_lines[3].pop(0)

        result = Aa_gun(
            name = " ".join(split_lines[0]),
            tier=split_lines[1][0],

            anti_air=split_lines[3][0],
            dmg=split_lines[3][2],
            rld=split_lines[3][3],
            rng=split_lines[3][5]
        )

        del data_lines[0:4]

        yield result


if __name__ == "__main__":
    for e in parse_equipment(Path("./data")):
        print(e)
