{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-parts,
    ...
  }:
    flake-parts.lib.mkFlake {inherit self;} {
      systems = [
        "x86_64-linux"
      ];

      perSystem = {
        pkgs,
        system,
        config,
        ...
      }: {
        _module.args.pkgs = import nixpkgs {
          inherit system;
        };

        packages = {
          azurlane-aa-env = pkgs.poetry2nix.mkPoetryEnv {
            projectDir = nixpkgs.lib.cleanSource ./.;
          };
        };

        devShells.default = pkgs.mkShellNoCC {
          LD_LIBRARY_PATH = nixpkgs.lib.makeLibraryPath [
            pkgs.stdenv.cc.cc
            pkgs.zlib
          ];
          packages = [
            pkgs.poetry
            pkgs.python3

            pkgs.gfortran
            pkgs.pkg-config
            pkgs.lapack-reference
          ];
          BLAS = "${pkgs.lapack-reference}/lib/libblas.so";
          LAPACK = "${pkgs.lapack-reference}/lib/liblapack.so";
        };
      };
    };
}
